
  var pasteText = document.querySelector("#output");
  pasteText.contentEditable = true;
  navigator.clipboard.readText().then(clipText =>{
    document.getElementById("output").innerText = clipText;
    QRCode.toCanvas(document.getElementById('canvas'), pasteText.textContent, function (error) {
      if (error) console.error(error)
      pasteText.parentNode.removeChild(pasteText);
    })
  });
