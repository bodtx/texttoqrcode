# texttoQrCode

Firefox Extension to transform text To Qrcode

## What it does ##

The extension includes:

* a javascript lib which generate the qrcode in an HTML canvas
* a little script that read the clipboard and give it to the qrcodeLib
* no malicious things

Limits:

* No image to qrcode ^^
* qrCode size is limited to firefox popup sir : 800*600
* No automatic copy, you have to manually copy what you want to transform (no bad magic).
* After 1000 caracters, generated QR code seem complicated to read (with my moto G3 2015).
